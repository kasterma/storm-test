storm-test
==========

Some basic tests running storm.

# Writing a storm topology to be submit to the AWS cluster.

With adgoji-cloudformation we have a cluster running on AWS.  Submit
our topologies to this cluster.

The cluster uses java-6, so set JAVA_HOME to home for java-6

    export JAVA_HOME=$(/usr/libexec/java_home -v1.6.*)

    lein clean
    lein compile
    lein uberjar

the stormtest jar is included in the uberjar.

*first try with storm starter example*

Worked with the following steps:
1. set correct java version
2. run above to get uberjar
3. set storm.yaml correct so that we can connect to storm
4. storm jar target/storm-starter-0.0.1-SNAPSHOT-standalone.jar storm.starter.clj.word_count countwords

*now use the same sequence on my own topology*

1. compare the clojure files (including project files) to see if there
are obvious differences.
    a. Fixed some differences in imports; midje caused problems with aot compilation.
2. use clojure 1.4.0 to get rid of exceptions as shown below.  This gets the basic
   example running.

    Exception in thread "main" java.lang.NoSuchMethodError: clojure.lang.RT.mapUniqueKeys([Ljava/lang/Object;)Lclojure/lang/IPersistentM
    ap;
            at stormtest.try3$loading__4910__auto__.invoke(try3.clj:1)
            at stormtest.try3__init.load(Unknown Source)
            at stormtest.try3__init.<clinit>(Unknown Source)
            at java.lang.Class.forName0(Native Method)
            at java.lang.Class.forName(Class.java:249)
            at clojure.lang.RT.loadClassForName(RT.java:2056)
            at clojure.lang.RT.load(RT.java:419)
            at clojure.lang.RT.load(RT.java:400)
            at clojure.core$load$fn__4890.invoke(core.clj:5415)
            at clojure.core$load.doInvoke(core.clj:5414)
            at clojure.lang.RestFn.invoke(RestFn.java:408)
            at clojure.lang.Var.invoke(Var.java:415)
            at stormtest.try3.<clinit>(Unknown Source)

3. now lein clean; lein compile; lein uberjar
4. the following command works
     storm jar target/stormtest-0.1.0-SNAPSHOT-standalone.jar stormtest.try3 stormtesttry3

# Getting storm cluster running locally

After the vagrant setup described below can start up a local cluster
with 1 zookeeper, 1 nimbus, and 3 slaves with storm running with the
command

    vagrant up

The storm UI can then be reached at

    http://localhost:8080/

Then download and unpack storm on the host machine:

    https://www.dropbox.com/s/fl4kr7w0oc8ihdw/storm-0.8.2.zip

The unzipped directory `storm-0.8.2` contains a bin directory.  This
needs to be place on your PATH.

Then create a .storm directory, and place the storm.yaml file there.
Set the nimbus server in there to localhost.

# Log of Vagrant Setup

Basic sources
[Michael Noll Tutorial Multi Node Setup](http://www.michael-noll.com/tutorials/running-multi-node-storm-cluster/)

[Vagrant Documentation](http://docs.vagrantup.com/v2/getting-started/index.html)

Future TODO:
1. Put zookeeper under supervision.
2. Some scripts (e.g. storm.sh) are not idempotent, meaning vagrant
   reload gives errors.

## Starting the machines and getting the network right

The hosts file to be put on all the machines is in
vagrant/resources/hosts.  My routers use 192.168.. address space, so
changing this to the 10... address space should avoid clashes (seems
default assiigned are in the 10.0.. range, so 10.10.. should still be
safe).

The script in vagrant/scripts/hostsfile.sh executed on all hosts makes
that they can all ping eachother.

## Adding java-6

Code in vagrant/scripts/java.sh

Also includes the apt-get update needed to get apt-get in good state.

## Zookeeper

Took the conf from the tutorial, but the install commands from
zookeeper getting started.  Using the instructions as provided in
the tutorial I verified zookeeper was up and running.

## ZeroMQ 2.1.7 and JZMQ

The process as described in the tutorial didn't quite work.  Found
a post on stackoverflow that gave me the commands that apparently
are missing from the makefiles http://superuser.com/questions/570248/error-building-jzmq-on-fedora-18-specifically-the-make-phase/579206#579206

## Storm setup

Getting the user set up and the storm app downloaded and installed
worked at in tutorial.

Adding storm.conf in the right place.  Ready to try running the cluster

## Supervisor

easy_install, copy the conf file

# Errors in running drpc

These seem somehow timbre related (when we stop using timbre the problems disappear).

When running the drpc topology with using timbre/info for logging:

doing this locally leads to the value 48205 getting repeated indefinitely.

doing this on AWS gave the following output:

    lein run -m stormtest.rpct --reqrpc
    14:51:20 INFO [stormtest.rpct] - seding request over drpc
    "CALLING"
    "44551"
    "DONE"
    lein run -m stormtest.rpct --reqrpc
    14:51:25 INFO [stormtest.rpct] - seding request over drpc
    "CALLING"
    "48205"
    "DONE"
    lein run -m stormtest.rpct --reqrpc
    14:51:32 INFO [stormtest.rpct] - seding request over drpc
    "CALLING"
    1    [main] ERROR org.apache.zookeeper.server.NIOServerCnxn  - Thread Thread[main,5,main] died
    DRPCExecutionException(msg:Request failed)
    				   at backtype.storm.generated.DistributedRPC$execute_result.read(DistributedRPC.java:887)
    				   at org.apache.thrift7.TServiceClient.receiveBase(TServiceClient.java:78)
    				   at backtype.storm.generated.DistributedRPC$Client.recv_execute(DistributedRPC.java:75)
    				   at backtype.storm.generated.DistributedRPC$Client.execute(DistributedRPC.java:61)
    				   at backtype.storm.utils.DRPCClient.execute(DRPCClient.java:54)
    				   at stormtest.rpct$show_value.invoke(rpct.clj:210)
    				   at stormtest.rpct$_main.doInvoke(rpct.clj:255)
    				   at clojure.lang.RestFn.invoke(RestFn.java:408)
    				   at clojure.lang.Var.invoke(Var.java:415)
    				   at user$eval5$fn__7.invoke(form-init8735920444905669916.clj:1)
    				   at user$eval5.invoke(form-init8735920444905669916.clj:1)
    				   at clojure.lang.Compiler.eval(Compiler.java:6511)
    				   at clojure.lang.Compiler.eval(Compiler.java:6501)
    				   at clojure.lang.Compiler.load(Compiler.java:6952)
    				   at clojure.lang.Compiler.loadFile(Compiler.java:6912)
    				   at clojure.main$load_script.invoke(main.clj:283)
    				   at clojure.main$init_opt.invoke(main.clj:288)
    				   at clojure.main$initialize.invoke(main.clj:316)
    				   at clojure.main$null_opt.invoke(main.clj:349)
    				   at clojure.main$main.doInvoke(main.clj:427)
    				   at clojure.lang.RestFn.invoke(RestFn.java:421)
    				   at clojure.lang.Var.invoke(Var.java:419)
    				   at clojure.lang.AFn.applyToHelper(AFn.java:163)
    				   at clojure.lang.Var.applyTo(Var.java:532)
    				   at clojure.main.main(main.java:37)
    make: *** [req-drpc] Error 1



## Another hint

When I removed timbre from the require but still had some references
to this namespace I got the following error when compiling

    $ lein compile
    Compiling stormtest.batches
    1    [main] ERROR org.apache.zookeeper.server.NIOServerCnxn  - Thread Thread[main,5,main] died
    java.lang.RuntimeException: No such namespace: timbre, compiling:(stormtest/batches.clj:9)
    			    at clojure.lang.Compiler.analyze(Compiler.java:6281)
    			    at clojure.lang.Compiler.analyze(Compiler.java:6223)
    			    at clojure.lang.Compiler$InvokeExpr.parse(Compiler.java:3497)
    			    at clojure.lang.Compiler.analyzeSeq(Compiler.java:6457)
    			    at clojure.lang.Compiler.analyze(Compiler.java:6262)
    			    at clojure.lang.Compiler.analyze(Compiler.java:6223)
    			    at clojure.lang.Compiler.compile1(Compiler.java:7030)
    			    at clojure.lang.Compiler.compile(Compiler.java:7097)
    			    at clojure.lang.RT.compile(RT.java:387)
    			    at clojure.lang.RT.load(RT.java:427)
    			    at clojure.lang.RT.load(RT.java:400)
    			    at clojure.core$load$fn__4890.invoke(core.clj:5415)
    			    at clojure.core$load.doInvoke(core.clj:5414)
    			    at clojure.lang.RestFn.invoke(RestFn.java:408)
    			    at clojure.core$load_one.invoke(core.clj:5227)
    			    at clojure.core$compile$fn__4895.invoke(core.clj:5426)
    			    at clojure.core$compile.invoke(core.clj:5425)
    			    at user$eval9.invoke(form-init9030099924187899279.clj:1)
    			    at clojure.lang.Compiler.eval(Compiler.java:6511)
    			    at clojure.lang.Compiler.eval(Compiler.java:6501)
    			    at clojure.lang.Compiler.load(Compiler.java:6952)
    			    at clojure.lang.Compiler.loadFile(Compiler.java:6912)
    			    at clojure.main$load_script.invoke(main.clj:283)
    			    at clojure.main$init_opt.invoke(main.clj:288)
    			    at clojure.main$initialize.invoke(main.clj:316)
    			    at clojure.main$null_opt.invoke(main.clj:349)
    			    at clojure.main$main.doInvoke(main.clj:427)
    			    at clojure.lang.RestFn.invoke(RestFn.java:421)
    			    at clojure.lang.Var.invoke(Var.java:419)
    			    at clojure.lang.AFn.applyToHelper(AFn.java:163)
    			    at clojure.lang.Var.applyTo(Var.java:532)
    			    at clojure.main.main(main.java:37)
    Caused by: java.lang.RuntimeException: No such namespace: timbre
           at clojure.lang.Util.runtimeException(Util.java:170)
           at clojure.lang.Compiler.resolveIn(Compiler.java:6736)
           at clojure.lang.Compiler.resolve(Compiler.java:6710)
           at clojure.lang.Compiler.analyzeSymbol(Compiler.java:6671)
           at clojure.lang.Compiler.analyze(Compiler.java:6244)
           ... 31 more
    Compilation failed: Subprocess failed