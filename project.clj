(defproject stormtest "0.1.0-SNAPSHOT"
  :description "PROVIDE DESCRIPTION"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/tools.cli "0.2.2"]
                 [midje "1.5.0"]]
  :aot :all
  :profiles {:dev
              {:dependencies [[storm "0.8.2"]]}})
