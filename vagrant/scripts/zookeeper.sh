wget http://apache.mirrors.hostinginnederland.nl/zookeeper/stable/zookeeper-3.4.5.tar.gz
tar xzf zookeeper-3.4.5.tar.gz
sudo mkdir /var/zookeeper
sudo chown vagrant /var/zookeeper/
cd zookeeper-3.4.5
cp /vagrant/vagrant/resources/zoo.cfg conf/
bin/zkServer.sh start
