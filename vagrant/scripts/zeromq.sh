sudo apt-get install -y gcc g++ uuid-dev libtool git pkg-config autoconf automake
cd /tmp
wget http://download.zeromq.org/zeromq-2.1.7.tar.gz
tar -xzf zeromq-2.1.7.tar.gz
cd zeromq-2.1.7
./configure
make
sudo make install

cd /tmp
git clone https://github.com/nathanmarz/jzmq.git
cd jzmq
export JAVA_HOME=/usr/lib/jvm/java-6-openjdk-i386
./autogen.sh
./configure
touch src/classdist_noinst.stamp
cd src
CLASSPATH=.:./.:$CLASSPATH javac -d . org/zeromq/ZMQ.java org/zeromq/ZMQException.java org/zeromq/ZMQQueue.java org/zeromq/ZMQForwarder.java org/zeromq/ZMQStreamer.java
cd ..
make
sudo make install
