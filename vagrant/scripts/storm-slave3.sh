# setup storm user
sudo groupadd -g 53001 storm
sudo mkdir -p /app/home/storm
sudo useradd -u 53001 -g 53001 -d /app/home/storm -s /bin/bash storm -c "Storm service account"
sudo chmod 700 /app/home/storm
sudo chage -I -1 -E -1 -m -1 -M -1 -W -1 -E -1 storm

# get storm 0.8.2
cd /tmp
wget https://dl.dropbox.com/u/133901206/storm-0.8.2.zip
cd /usr/local
sudo unzip /tmp/storm-0.8.2.zip
sudo chown -R storm:storm storm-0.8.2
sudo ln -s storm-0.8.2 storm

# create local working dir (why no use home dir?)
sudo mkdir -p /app/storm
sudo chown -R storm:storm /app/storm
sudo chmod 750 /app/storm

# put the storm config in the right place
sudo cp /vagrant/vagrant/resources/storm-slave3.yaml /usr/local/storm/conf/storm.yaml
