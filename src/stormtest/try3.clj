(ns stormtest.try3
  (:import [backtype.storm StormSubmitter LocalCluster])
  (:use [backtype.storm clojure config])
  (:gen-class))

(defspout numbers-spout ["start"] {:prepare true}
  [conf context collector]
  (let [idx      (atom 0)]
    (spout
     (nextTuple []
       (Thread/sleep 500)
       (let [num        (swap! idx inc)]
         (emit-spout! collector [num]))))))

(defbolt pause-bolt ["stop"]
  [tuple collector]
  (Thread/sleep 1000)
  (emit-bolt! collector [(second (first tuple))])
  (ack! collector tuple))

(defbolt start-stop-bolt ["running" "randit"] {:prepare true}
  [conf context collector]
  (let [running (atom #{})]
    (bolt
     (execute [tuple]
       (let [source (.getSourceComponent tuple)
             id  (.getLong tuple 0)]
         (if (= "stop" source)
           (do
             (let [new-running (swap! running #(disj % id))]
               (emit-bolt! collector [new-running (rand-int 200)])))
           (do
             (let [new-running (swap! running #(conj % id))]
               (emit-bolt! collector [new-running (rand-int 100)]))
             (ack! collector tuple))))))))

(defbolt count-set-bolt ["count" "randit"]
  [tuple collector]
  (emit-bolt! collector [(count (second (first tuple))) (rand-int 300)]))

(defn mk-topology []
  (topology
   {"start" (spout-spec numbers-spout)}
   {"stop" (bolt-spec {"start" :shuffle}
                       pause-bolt
                       :p 6)
    "running" (bolt-spec {"start" ["start"]
                          "stop" ["stop"]}
                          start-stop-bolt
                          :p 2)
    "count" (bolt-spec {"running" :shuffle}
                       count-set-bolt)}))

(defn run-local! []
  (let [cluster (LocalCluster.)]
    (.submitTopology cluster "sum-the-nums" {TOPOLOGY-DEBUG true} (mk-topology))
    (Thread/sleep 20000)
    (.shutdown cluster)
    ))

(defn submit-topology! [name]
  (StormSubmitter/submitTopology
   name
   {TOPOLOGY-DEBUG true
    TOPOLOGY-WORKERS 3}
   (mk-topology)))

(defn -main
  ([]
     (println "ho ho ho")
     (run-local!))
  ([name]
     (submit-topology! name)))
