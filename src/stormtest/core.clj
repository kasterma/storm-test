(ns stormtest.core
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.tools.cli :as cli]
            [clojure.tools.logging :refer [trace info spy]]
            [clojure.pprint :as pp])
  (:import [backtype.storm StormSubmitter LocalCluster])
  (:use [backtype.storm clojure config])
  (:gen-class))


(defspout numbers-spout ["number"] {:params [numbers] :prepare true}
  [conf context collector]
  (let [sum-send (atom 0)
        idx      (atom 0)
        messages (atom {})]
    (spout
     (nextTuple []
                (trace "numbers-spout called")
                (Thread/sleep 500)
                (let [next-num  (rand-nth numbers)
                      id        (swap! idx inc)]
                  (emit-spout! collector [next-num] :id id)
                  (swap! messages #(assoc % id next-num))
                  (swap! sum-send #(+ % next-num)))
                (trace "total send:" @sum-send))
     (ack [id]
          (swap! messages #(dissoc % id))
          (trace "messages:" @messages))
     (fail [id]
           (let [msg (@messages id)]
             (trace "resending" id msg)
             (trace "total send:" @sum-send)
             (trace "messages:" @messages)
             (emit-spout! collector [msg] :id id))))))

(defbolt sum-bolt ["running-sum"] {:prepare true}
  [conf context collector]
  (let [rs (atom 0)]
    (bolt
     (execute [tuple]
              (if false; (< (rand) 0.9) ; false means perfectly reliable, o/w can adjust reliability
                (do
                  (fail! collector tuple)
                  (trace "rs:" @rs))
                (do
                  (trace "execute sum-bolt" tuple)
                  (let [new-sum (swap! rs #(+ % (spy (second (first tuple)))))]
                    (emit-bolt! collector [new-sum]))
                  (ack! collector tuple)))))))

(defn mk-topology []
  (topology
   {"nums-sp" (spout-spec (numbers-spout [11 22 33 44 55])
                          :p 2)}
   {"summit-b" (bolt-spec {"nums-sp" :shuffle}
                          sum-bolt
                          :p 2)}))

(defn run-local! []
  (let [cluster (LocalCluster.)]
    (.submitTopology cluster "sum-the-nums" {TOPOLOGY-DEBUG true} (mk-topology))
    (Thread/sleep 20000)
    (.shutdown cluster)
    ))

(defn submit-topology! [name]
  (StormSubmitter/submitTopology
   name
   {TOPOLOGY-DEBUG true
    TOPOLOGY-WORKERS 3}
   (mk-topology)))

(defn -main
  ([]
     (println "ho ho ho")
     (run-local!))
  ([name]
     (submit-topology! name)))
