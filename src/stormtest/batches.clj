(ns stormtest.batches
  "A storm topology that uses batches.  Having a bolt collect several
   inputs, and only then pass a result on."
  (:require [clojure.tools.logging :refer [info]])
  (:import [backtype.storm StormSubmitter LocalCluster])
  (:use [backtype.storm clojure config])
  (:gen-class))

;; spout that outputs the natural numbers one at a time
(defspout numbers-spout ["startt"] {:prepare true}
  [conf context collector]
  (let [idx      (atom 0)]
    (spout
     (nextTuple []
       (Thread/sleep 500)
       (let [num        (swap! idx inc)]
         (emit-spout! collector [num]))))))


;; bolt that adds up the numbers it gets as input, and outputs
;; partial sums as soon as they get above 100
(defbolt initial-add-bolt ["partial-sums"] {:prepare true}
  [conf context collector]
  (let [partial-sum    (atom {:id "initial" :send 0 :recv 0})]
    (bolt
     (execute [tuple]
       (info tuple)
       (let [i  (.getLong tuple 0)]
         (swap! partial-sum update-in [:recv] (partial + i))
         (info @partial-sum)
         (ack! collector tuple)
         (let [recv  (get @partial-sum :recv)]
           (when (< 100 recv)
             (swap! partial-sum #(update-in (update-in % [:send] (partial + recv)) [:recv] (partial - recv)))
             (info @partial-sum)
             (emit-bolt! collector [recv]))))))))

;; bolt that sums its inputs, and outputs this sum
(defbolt final-add-bolt ["sum"] {:prepare true}
  [conf context collector]
  (let [full-sum    (atom {:id "final" :recv 0})]
    (bolt
     (execute [tuple]
       (info tuple)
       (let [i (.getLong tuple 0)]
         (swap! full-sum update-in [:recv] (partial + i))
         (info @full-sum)
         (emit-bolt! collector [i])
         (ack! collector tuple))))))

;;
;;       +--------------------+            +--------------------+         +-------------------+
;;       |                    |            |                    |         |                   |
;;       |                    |----------->|                    |-------->|                   |---->
;;       |    start           |            |    partial         |         |     final         |
;;       |                    |            |                    |         |                   |
;;       +--------------------+            +--------------------+         +-------------------+
;;

(defn mk-topology []
  (topology
   {"start" (spout-spec numbers-spout)}
   {"partial" (bolt-spec {"start" :shuffle}
                         initial-add-bolt)
    "final" (bolt-spec {"partial" :shuffle}
                       final-add-bolt)}))

(defn run-local! []
  (let [cluster (LocalCluster.)]
    (.submitTopology cluster "sum-the-nums" {TOPOLOGY-DEBUG true} (mk-topology))
    (Thread/sleep 20000)
    (.shutdown cluster)
    ))

(defn submit-topology! [name]
  (StormSubmitter/submitTopology
   name
   {TOPOLOGY-DEBUG true
    TOPOLOGY-WORKERS 3}
   (mk-topology)))

(defn -main
  ([]
     (println "running locally")
     (run-local!))

  ([name]
     (println "submitting")
     (submit-topology! name)))
