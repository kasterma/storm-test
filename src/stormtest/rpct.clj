(ns stormtest.rpct
  "A storm topology that collects status in a bolt that can then
   be read out via an rpc call."
  (:require [clojure.tools.cli :refer [cli]]
            [clojure.tools.logging :refer [info]])
  (:import [backtype.storm StormSubmitter LocalCluster LocalDRPC])
  (:import [backtype.storm.drpc ReturnResults DRPCSpout])
  (:import [backtype.storm.utils DRPCClient])
  (:use [backtype.storm clojure config])
  (:gen-class))

;; spout that outputs the natural numbers one at a time
(defspout numbers-spout ["startt"] {:prepare true}
  [conf context collector]
  (let [idx      (atom 0)]
    (spout
     (nextTuple []
       (Thread/sleep 500)
       (let [num        (swap! idx inc)]
         (emit-spout! collector [num]))))))


;; bolt that adds up the numbers it gets as input, and outputs
;; partial sums as soon as they get above 100
(defbolt initial-add-bolt ["partial-sums"] {:prepare true}
  [conf context collector]
  (let [partial-sum    (atom {:id "initial" :send 0 :recv 0})]
    (bolt
     (execute [tuple]
       (info tuple)
       (let [i  (.getLong tuple 0)]
         (swap! partial-sum update-in [:recv] (partial + i))
         (info @partial-sum)
         (ack! collector tuple)
         (let [recv  (get @partial-sum :recv)]
           (when (< 100 recv)
             (swap! partial-sum #(update-in (update-in % [:send] (partial + recv)) [:recv] (partial - recv)))
             (info @partial-sum)
             (emit-bolt! collector [recv]))))))))

;; bolt that sums its inputs, and when requested outputs this sum
(defbolt final-add-bolt ["result" "return-info"] {:prepare true}
  [conf context collector]
  (let [full-sum    (atom {:id "final" :recv 0})]
    (bolt
     (execute [tuple]
       (info tuple)
       (let [source  (.getSourceComponent tuple)]
         (if (= source "show")
           (do
             (info "EMEMEMEMEMEMMEEEMEMEMEMEMEME")  ;; to recognize in the flowing output
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (info @full-sum)
             (emit-bolt! collector [(str (get @full-sum :recv)) (.getValue tuple 1)])
             (ack! collector tuple))
           (let [i (.getLong tuple 0)]
             (swap! full-sum update-in [:recv] (partial + i))
             (info @full-sum)
             (ack! collector tuple))))))))

(defspout request-sum ["req" "req-id"] {:prepare true}
  [conf context collector]
  (spout
   (nextTuple []
              (Thread/sleep 5000)
              (emit-spout! collector [:show-total 1]))))

;;
;;       +--------------------+            +--------------------+         +-------------------+
;;       |                    |            |                    |         |                   |
;;       |                    |----------->|                    |-------->|                   |---->
;;       |    start           |            |    partial         |         |     final         |
;;       |                    |            |                    |         |                   |
;;       +--------------------+            +--------------------+         +-------------------+
;;                                                                           /--
;;                                       +--------------------+          /---
;;                                       |                    |      /---
;;                                       |    show            |  /---
;;                                       |                    |--
;;                                       +--------------------+
;;

(defn mk-topology []
  (topology
   {"start" (spout-spec numbers-spout)
    "show" (spout-spec request-sum)}
   {"partial" (bolt-spec {"start" :shuffle}
                         initial-add-bolt)
    "final" (bolt-spec {"partial" :shuffle
                        "show" :shuffle}
                       final-add-bolt)}))


;;
;;       +--------------------+            +--------------------+         +-------------------+      +-------------------+
;;       |                    |            |                    |         |                   |      |                   |
;;       |                    |----------->|                    |-------->|                   |----> |                   |
;;       |    start           |            |    partial         |         |     final         |      |     returnb       |
;;       |                    |            |                    |         |                   |      |           (drpc)  |
;;       +--------------------+            +--------------------+         +-------------------+      +-------------------+
;;                                                                           /--
;;                                       +--------------------+          /---
;;                                       |                    |      /---
;;                                       |    show            |  /---
;;                                       |      (drcp)        |--
;;                                       +--------------------+
;;


(defn mk-topology-drpc
  "Make the topology for drpc use."
  ([drpc]
     (topology
      {"start" (spout-spec numbers-spout)
       "show" (spout-spec (DRPCSpout. "show-drpc" drpc))}
      {"partial" (bolt-spec {"start" :shuffle}
                            initial-add-bolt)
       "final" (bolt-spec {"partial" :shuffle
                           "show" :shuffle}
                          final-add-bolt)
       "returnb" (bolt-spec {"final" :shuffle}
                            (ReturnResults.))}))
  ([]
     (topology
      {"start" (spout-spec numbers-spout)
       "show" (spout-spec (DRPCSpout. "show-drpc"))}
      {"partial" (bolt-spec {"start" :shuffle}
                            initial-add-bolt)
       "final" (bolt-spec {"partial" :shuffle
                           "show" :shuffle}
                          final-add-bolt)
       "returnb" (bolt-spec {"final" :shuffle}
                            (ReturnResults.))})))


(defn run-local!
  "run local storm with no drpc enabled"
  []
  (let [cluster (LocalCluster.)]
    (.submitTopology cluster "sum-the-nums" {TOPOLOGY-DEBUG true} (mk-topology))
    (Thread/sleep 20000)
    (.shutdown cluster)))

(defn run-local-drpc!
  "run local storm with a drpc server"
  []
  (let [cluster (LocalCluster.)
        drpc    (LocalDRPC.)]
    (.submitTopology cluster "sum-the-nums" {TOPOLOGY-DEBUG true} (mk-topology-drpc drpc))
    (Thread/sleep 10000)
    (info "CALLING CALLING")
    (loop [i 1]
      (info (str "RESULT " i " RESULT" (.execute drpc "show-drpc" "1")))
      (Thread/sleep 10000)
      (recur (inc i)))
    (info "CALLING CALLING")
    (info (str "RESULT RESULT" (.execute drpc "show-drpc" "1")))
    (Thread/sleep 10000)
    (info "CALLING CALLING")
    (info (str "RESULT RESULT" (.execute drpc "show-drpc" "1")))
    (Thread/sleep 10000)
;    (.shutdown cluster)
;    (.shutdown drpc)
    ))


(defn submit-topology! [name]
  (StormSubmitter/submitTopology
   name
   {TOPOLOGY-DEBUG true
    TOPOLOGY-WORKERS 3}
   (mk-topology)))

(defn submit-topology-drpc! [name]
  (StormSubmitter/submitTopology
   name
   {TOPOLOGY-DEBUG true
    TOPOLOGY-WORKERS 3}
   (mk-topology-drpc)))

(defn show-value
  "Make the drpc call to get the current sum"
  []
  (let [client   (DRPCClient. "nimbus.vpn.adgoji.com" 3772)]
    (prn "CALLING")
    (prn (.execute client "show-drpc" "1"))
    (prn "DONE")))

(defn -main [& args]
  (let [[o a b]
        (cli args
             ["--local"
              "Run the topology locally with simulated times for drpc reqeusts"
              :flag true]
             ["--localdrpc"
              "Run the topology locally with actual drpc calls"
              :flag true]
             ["--remote"
              "Run the topology remote with simulated times for drpc requests"
              :flag true]
             ["--remotedrpc"
              "Run the topology remote with drpc server"
              :flag true]
             ["--reqrpc"
              "query the remote drpc to get the result"
              :flag true])]
    (cond
     (:local o)
     (do
       (info "running locally")
       (run-local!))

     (:localdrpc o)
     (do
       (info "running locally with drpc")
       (run-local-drpc!))

     (:remote o)
     (do
       (info "submitting")
       (submit-topology! "testing-nodrpc"))

     (:remotedrpc o)
     (do
       (info "running remote with drpc")
       (submit-topology-drpc! "testing-drpc"))

     (:reqrpc o)
     (do
       (info "seding request over drpc")
       (show-value)))))
