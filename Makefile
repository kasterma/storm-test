setup:
	mkdir -p ~/.storm/
	cp vagrant/resources/storm-host.yaml ~/.storm/storm.yaml

run-batches:
	lein compile
	lein uberjar
	storm jar target/stormtest-0.1.0-SNAPSHOT-standalone.jar stormtest.batches testrun

run-drpc-local:
	lein run -m stormtest.rpct --localdrpc

run-drpc:
	lein compile
	lein uberjar
	storm jar target/stormtest-0.1.0-SNAPSHOT-standalone.jar stormtest.rpct --remotedrpc

req-drpc:
	lein run -m stormtest.rpct --reqrpc
